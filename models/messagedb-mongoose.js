if(process.env.VCAP_SERVICES){
  var env = JSON.parse(process.env.VCAP_SERVICES);
  var mongo = env['mongodb-1.8'][0]['credentials'];
}
else{
  // mongodb://<user>:<password>@staff.mongohq.com:10006/riversbendlodge  -- MongoHQ database
  // mongodb://"":""@localhost:27017/db  --  local mongodb on mac
  var mongo = {
    "hostname":"staff.mongohq.com",
    "port":10006,
    "username":"riversbendlodge",
    "password":"Montana137",
    "name":"",
    "db":"riversbendlodge"
  }
}

var generate_mongo_url = function(obj){
  obj.hostname = (obj.hostname || 'localhost');
  obj.port = (obj.port || 27017);
  obj.db = (obj.db || 'db');

  if(obj.username && obj.password){
    return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
  else{
    return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
  }
}

var dburl = generate_mongo_url(mongo);

var util = require('util');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

exports.connect = function(callback) {
    mongoose.connect(dburl);
}

exports.disconnect = function(callback) {
    mongoose.disconnect(callback);
}

exports.setup = function(callback) { callback(null); }

var MessageSchema = new Schema({
    name     : String,
    email    : String,
    message     : String,
    ts       : { type: Date, default: Date.now }
    });
mongoose.model('Message', MessageSchema);

var Message = mongoose.model('Message');

exports.emptyMessage = { "_id": "", name: "", email: "", note: "" };

exports.add = function(name, email, message, callback) {
    var newMessage = new Message();
    newMessage.name = name;
    newMessage.email = email;
    newMessage.message = message;
    newMessage.save(function(err) {
        if(err) {
            util.log('FATAL '+err);
            callback(err);
        } else
            callback(null);
    });
}