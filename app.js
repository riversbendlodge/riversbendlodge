// config for cloud9, cloudfoundry and local
// version 0.9
var port = (process.env.VCAP_APP_PORT || process.env.C9_PORT || 3001);
var host = (process.env.VCAP_APP_HOST || '0.0.0.0' || 'localhost');
var express = require('express');
var mailer = require('./controllers/mailer');

var app = express.createServer(
    // express.logger()
);

// Set up ejs as the view engine
app.register('.ejs', require('ejs'));
app.set('views', __dirname + '/views/templates');
app.set('view engine', 'ejs');
app.set('view options', {layout: false});

app.configure(function() {
    app.use(app.router);
    app.use(express.static(__dirname + '/views'));
    //app.use(express.errorHandler({
    //  dumpExceptions: true, showStack: true }));
});

app.get('/sendMail', function (req, res) {
    mailer.send(req, res);
     // res.send("hello");
});

app.error(function(err, req, res, next) {
    res.rendor('error.ejs', {errorMessage: err, req: req});
});

app.listen(port, host);

console.log('Listening to HTTP on ' + host + ':' + port);