﻿function sendMail() {
    var theForm = document.forms["contact_form"];
    var name = theForm.elements["name"].value;
    var email = theForm.elements["email"].value;
    var message = theForm.elements["message"].value;
    var myJSONObject = {"name":name, "email":email, "message":message};
    // alert(name + email + message);
    if (email.search("@") > 0) {
    
        var btnSend = document.getElementById("btnSend");
        btnSend.innerHTML = "<span>Sending...</span>";
        
        <!--
        //JSON POST
        $.ajax('/sendMail', {
            type: 'POST',
            data: JSON.stringify(myJSONObject),
            contentType: 'text/json',
            success: function(msg) { 
                btnSend.innerHTML = "<span>Send Message</span>";
                if (msg == "success") { alert("Thank you! We'll contact you as soon as possible."); } else { alert("Sorry! We're updating our software. Please email jeff@riversbendlodge.com for your request and we will get back to you promptly."); }
            },
            error  : function() { 
                btnSend.innerHTML = "<span>Send Message</span>";
                alert("Something bad happened on the server. Try again or call us! Sorry"); 
            }
        });
        //-->
    
        
        // query format of passed data
        $.ajax({
            url: "/sendMail",
            global: false,
            type: "GET",
            data: "name=" + name + "&email=" + email + "&message=" + message,
            dataType: "text",
            async: true,

            success: function (msg) {
                btnSend.innerHTML = "<span>Send Message</span>";
                if (msg == "success") { alert("Thank you! We'll contact you as soon as possible."); } else { alert("Sorry! We're updating our software. Please email jeff@riversbendlodge.com for your request and we will get back to you promptly."); }
            }
        });
        
        
    }
    else { alert("Please enter a valid email address."); }
}